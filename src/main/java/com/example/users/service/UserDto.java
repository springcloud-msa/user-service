package com.example.users.service;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class UserDto implements Serializable {
	private String email;
	private String name;
	private String password;
	private String userId;
	private String encryptedPassword;
	
	@Builder
	public UserDto(String email, String name, String password) {
		super();
		this.email = email;
		this.name = name;
		this.password = password;
	}

	@Builder
	public UserDto(String email, String name, String userId, String encryptedPassword) {
		this.email = email;
		this.name = name;
		this.userId = userId;
		this.encryptedPassword = encryptedPassword;
	}

	
}
