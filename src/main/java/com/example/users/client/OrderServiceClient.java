package com.example.users.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="order-service") //application name
public interface OrderServiceClient {

	@GetMapping("/order-service/{userId}/orders")//요청 URI
	String getOrder(@PathVariable String userId); //abstract method 정의
}
