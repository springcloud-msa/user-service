package com.example.users.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.mindrot.jbcrypt.BCrypt;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.example.users.exception.UnAuthenticationException;
import com.example.users.repository.UserEntity;
import com.example.users.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
    
	UserRepository userRepository;
	ModelMapper mapper;
	
	public UserServiceImpl(UserRepository userRepository,ModelMapper mapper) {
		this.userRepository = userRepository;
		this.mapper = mapper;
	}


	/**
	 * 사용자 등록 
	 * UUID.randomUUID로 userId생성
	 * password 암호화해서 encryptedPassword 생성
	 * userDto ->userEntity로 변환해서 reposiotry 호출
	 * @param userDto
	 * @return 
	 */
	@Override
	public UserDto createUser(UserDto userDto) {
		//controller userDto(email, name, password)
		// userid, encryptedPassword 
		userDto.setUserId(UUID.randomUUID().toString());
		//bcrypt로  암호화해서
		userDto.setEncryptedPassword( BCrypt.hashpw(userDto.getPassword(), BCrypt.gensalt()));  
		//user dto->userEntity변환
		userRepository.save(mapper.map(userDto, UserEntity.class));
		return userDto;
	}

	/**
	 * 사용자 목록
	 * @return Iterable<UserEntity> List<UserDto> 타입의 객체로 리턴
	 */
	@Override
	public List<UserDto> getUsers() {
		List<UserDto> users = new ArrayList<>();
		userRepository.findAll().forEach(
				entity -> users.add(mapper.map(entity, UserDto.class))
		);
		return users;
	}

	@Override
	public UserDto getUser(String userId) {
		return mapper.map(userRepository.findByUserId(userId), UserDto.class);
	}

	@Override
	public String deleteUser(String userId) {
		userRepository.delete( userRepository.findByUserId(userId));
		return userId;
	}

	@Override
	public UserDto updateUser(UserDto userDto) {
		UserEntity userEntity = userRepository.findByUserId(userDto.getUserId());
		userEntity.setName(userDto.getName());
		userEntity.setEncryptedPassword(BCrypt.hashpw(userDto.getPassword(), BCrypt.gensalt()));
		
		userRepository.save(userEntity);
		
		return mapper.map(userEntity, UserDto.class);
	}

	@Override
	public UserDto loginCheck(UserDto user) 
			throws UnAuthenticationException {
		// user email로 검색
		UserEntity userEntity= 
				userRepository.findByEmail(user.getEmail());
		// repository-> encryptedPassword 체크
		if(userEntity !=null 
				&& BCrypt.checkpw(user.getPassword(), userEntity.getEncryptedPassword())) {
			return mapper.map(userEntity, UserDto.class);
		}
		throw new UnAuthenticationException();
	}
	
}
