package com.example.users.exception;

public class UnAuthenticationException extends Exception {

	public UnAuthenticationException() {
		super("아이디 비밀번호를 확인하세요.");
	}

	public UnAuthenticationException(String message) {
		super(message);
	}
}
