package com.example.users.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.crypto.SecretKey;

import org.modelmapper.ModelMapper;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.users.client.OrderServiceClient;
import com.example.users.exception.UnAuthenticationException;
import com.example.users.service.UserDto;
import com.example.users.service.UserService;
import com.example.users.vo.RequestLoginUser;
import com.example.users.vo.RequestUser;
import com.example.users.vo.ResponseUser;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user-service")
@Slf4j
public class UserController {
	Environment env;
	UserService service;
	ModelMapper mapper;
	OrderServiceClient orderServiceClient;
	
//	@Value("${greeting.message}")
//	String greeting;
	/**
	 * SpringContainer가 생성해준 environment객체 생성자로 Injection
	 * @param env
	 */
	public UserController(Environment env, 
						ModelMapper mapper, 
						UserService service,
						OrderServiceClient orderServiceClient) {
		this.env = env;
		this.mapper = mapper;
		this.service = service;
		this.orderServiceClient = orderServiceClient;
	}


	/**
	 *   /user-service/health_check 요청 시  application.yml의 server.port 정보로 연결 여부 응답
	 * @return 연결포트 성공
	 */
	@GetMapping("/health-check")
	public String health_check(HttpServletRequest request) {
		return String.format("%s Connected Success!!! %s", 
							 env.getProperty("spring.application.name"),
				             request.getServerPort());
	}
	
	@PostMapping("/users")
	public ResponseEntity<ResponseUser> singUp(@RequestBody RequestUser user){
		//valid check(RequestUser validation framework) -> requestuser:userdto로 매핑, service 
		//-> 데이터 userdto:responseuser 리턴
		UserDto userDto = mapper.map(user, UserDto.class);
		UserDto resultUserDto = service.createUser(userDto);
		ResponseUser responseUser = mapper.map(resultUserDto, ResponseUser.class);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(responseUser);

	}
	
	@GetMapping("/users")
	public ResponseEntity<List<ResponseUser>> getUsers(){
		List<ResponseUser> responseUsers = new ArrayList<>();
		service.getUsers().forEach(
				 userDto -> responseUsers.add(mapper.map(userDto, ResponseUser.class))
				);
		return ResponseEntity.status(HttpStatus.OK).body(responseUsers);
	}
	
	@GetMapping("/users/{userId}")
	public ResponseEntity<ResponseUser> getUser(@PathVariable String userId){
		UserDto userDto = service.getUser(userId);
		
		//FeignClient로 다른 서비스 사용하기
		String orders  = orderServiceClient.getOrder(userId);
		log.info("======= Feign Client : 사용자 주문정보 "+ orders);
		
		ResponseUser responseUser = mapper.map(userDto, ResponseUser.class);
		return ResponseEntity.status(HttpStatus.OK).body(responseUser);
	}
	
	@DeleteMapping("/users/{userId}")
	public ResponseEntity<String> deleteUser(@PathVariable String userId){
		
		service.deleteUser(userId);
		
		return ResponseEntity.status(HttpStatus.OK).body(userId+" remove success!! ");
	}
	
	@PutMapping("/users/{userId}")
	public ResponseEntity<ResponseUser> updateUser(@PathVariable String userId, 
			                                   @RequestBody RequestUser user){
		
		UserDto userDto = mapper.map(user, UserDto.class);
		userDto.setUserId(userId);
			
	    userDto = service.updateUser(userDto);	
		
	    ResponseUser responseUser = mapper.map(userDto, ResponseUser.class);
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(responseUser);
	}
	
	@PostMapping("/auth/signin")
	public ResponseEntity<?> signin(@RequestBody RequestLoginUser user){
		ResponseEntity responseEntity=null;
		UserDto userDto=null;
	    String accessToken=null;
		try {
			userDto = service.loginCheck(mapper.map(user, UserDto.class));
			byte[] secretKeyBytes = Base64.getEncoder().encode(env.getProperty("token.secret").getBytes());
	        SecretKey secretKey = Keys.hmacShaKeyFor(secretKeyBytes);

	        Instant now = Instant.now();

	        String token = Jwts.builder()
		                .subject(userDto.getUserId())
		                .expiration(Date.from(now.plusMillis(Long.parseLong(env.getProperty("token.expiration_time")))))
		                .issuedAt(Date.from(now))
		                .signWith(secretKey)
		                .compact();
	
	        responseEntity=ResponseEntity.status(HttpStatus.ACCEPTED)
					 .header("access-token", token)
					 .header("user-id", userDto.getUserId())
					 .body(mapper.map(userDto, ResponseUser.class));
		} catch (UnAuthenticationException e) {
			responseEntity = exceptionHandler(e);
		}
		
		return responseEntity ;		
	}
	
	
	private ResponseEntity<String> exceptionHandler(Exception error){
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error.getMessage());
	}
	
}


